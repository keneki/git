<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}

 $profList=getAllProf();
 $deptList=getAllDept();
 
?>
<!DOCTYPE html>
<html>

<head>
<?php include 'head.php'?>
</head>
   <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> -->
<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
        <form class="form-horizontal" action="" method="post" name="uploadCSV"
            enctype="multipart/form-data">
        <div class="input-row">
        <a class="col-md-4 control-label">Choose CSV File</a> <input
            type="file" name="file" id="file" accept=".csv">
        <button type="submit" class="btn btn-primary" id="submit" name="import"
            class="btn-submit">Import</button>
        <br />

        </div>
        <div id="labelError"></div>
        </form>
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Instructors</a>
        </div>
        <div class="section" style="margin: 2rem;">
        <?php
                if($_GET['status']=="success_delete"){echo "<div class='card-panel teal lighten-5 teal-text'>Deleted sucessfully</div>";}
                if($_GET['status']=="successInstructor"){echo "<div class='card-panel green lighten-5 green-text'>Added sucessfully</div>";}
                if($_GET['status']=="failedInstructor"){echo "<div class='card-panel red lighten-5 red-text'>An error Occured</div>";}
        ?>
        <?php
        $conn = mysqli_connect("localhost", "root", "", "ams_db");
        if (isset($_POST["import"])) {

        $fileName = $_FILES["file"]["tmp_name"];

        if($_FILES["file"]["size"] > 0) {

        $file = fopen($fileName, "r");
        
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $sqlInsert = "INSERT into tbl_prof (prof_uid,profFirstname,profLastname,profCardId,profIdNum,dept_name)
                   values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $column[3] . "','" . $column[4] . "','" . $column[5] . "')";
            $result = mysqli_query($conn, $sqlInsert);
            
            if (! empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
        }
    }
}
?>

<?php
$sqlSelect = "SELECT * FROM tbl_prof";
$result = mysqli_query($conn, $sqlSelect);
if (mysqli_num_rows($result) > 0) {
?>
        
            <table id="tbl_ins" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- concat intructor name -->
                        <th>ID Number</th>
                        <th>Name</th>
                        <th>Department</th>
                        <!-- concat time start and end -->
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
	                while ($profList = mysqli_fetch_array($result)) {
                ?>
                <tbody>
                
                    
                    <tr>
                       <!-- echo $profData['profCardId'] -->
                        <td><?php echo $profList['profIdNum']?></td>
                        <td><?php echo $profList['profLastname'].', '.$profList['profFirstname']?></td>
                        <td><?php echo $profList['dept_name']?></td>
                        <td>
                            <!-- <a class="waves-effect waves-light btn"
                                href="editInstructor.php?id=<?php echo $profList['prof_uid']?>"><i
                                    class="material-icons">create</i></a> -->
                            <button onclick="test(<?php echo $profList['prof_uid']?>)"
                                class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                    <?php
                        }
                    ?>
                </tbody>
            </table>

        
        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Instructor</h4>
            <div class="row">
                <form action="controller/instructorCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <input id="profCardId" name="profCardId" type="text" pattern="[0-9]+" title="Please input number only" class="validate">
                        <label for="profCardId">Card ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profFirstname" name="profFirstname" type="text" pattern="^[A-Za-z -']+$" title="Letters, Whitespace, ' and - only" class="validate">
                        <label for="profFirstname">First Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profLastname" name="profLastname" type="text" pattern="^[A-Za-z -']+$" title="Letters, Whitespace, ' and - only" class="validate">
                        <label for="profLastname">Last Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profIdNum" name="profIdNum" type="text" pattern="[0-9 -]+" title="Please input number only" class="validate">
                        <label for="profIdNum">ID Number</label>
                    </div>
                    <div class="input-field col s12">
                    <select name="dept_name">
                            <option value="" disabled selected>Department</option>
                            <?php
                                foreach($deptList as $deptData){
                            ?>
                            <option value="<?php echo $deptData['dept_name']?>">
                                <?php echo $deptData['dept_name']?> </option>
                            <?php
                                }
                            ?>
                            
                    </select>
                    </div>

            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddProf">Add
                Instructor
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#tbl_ins').DataTable(
                {
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },

                    {
                        text: 'Export to PDF',
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                    {
                    text: 'Import CSV',
                    action: function () {
                    uploadEditor.create( {
                        title: 'CSV file import'
                    } );
                }
            }
                ]
            });
        }
        );
    </script> -->
    
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if(result.value){
                    window.location.href = `controller/deleteInstructor.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>