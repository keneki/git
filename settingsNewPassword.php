<?php
include 'model/dbhelper.php';
if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}

$courseList = getAllCourse();
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <div class="CIT-image">
                <div class="container">
                    <div class="section  card-panel z-depth-4" style="border-radius: 20px;margin-top: 10%;padding: 50px 75px;">
                        <div class="container">

                        </div>
                        <form action="controller/newpassforgot.php" method="POST">

                            <div class="input-field col s12">
                                <input id="username" type="text" name="username" value="<?php echo $_GET['username'] ?>" hidden>
                                <input id="userId" type="number" name="userId" value="<?php echo $_GET['id'] ?>" hidden>
                                <input id="newpassword" type="password" name="newpassword">
                                <label class="active" for="newpassword">New Password</label>
                            </div>
                            <div class="input-field col s12">
                                <input id="cnewpassword" type="password" name="cnewpassword">
                                <label class="active" for="cnewpassword">Confirm New Password</label>
                            </div>

                            <button id="btnSettingsNewPassword" name="btnSettingsNewPassword" class="btn waves-effect waves-light " type="submit" name="action">Done
                                <i class="material-icons right">send</i>
                            </button>
                        </form>

                    </div>
                </div>
            </div>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->
    <!-- <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Course</h4>
            <div class="row">
                <form action="controller/courseCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <input id="subject_code" name="subject_code" type="text" pattern="[a-zA-Z0-9\s]+" class="validate">
                        <label for="subject_code">Subject Code</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="desc_title" name="desc_title" type="text" pattern="^[A-Za-z0-9 ,/]+$" class="validate">
                        <label for="desc_title">Descriptive Title</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="section" name="section" type="text" pattern="[a-zA-Z0-9]+" class="validate">
                        <label for="section">Section</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="units" name="units" type="text" pattern="[0-9]+" title="Please input number only" class="validate">
                        <label for="units">Units</label>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddCourse">Add
                Course
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div> -->
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    {
                        text: 'Export to PDF',
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script> -->
    <!-- <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `controller/deleteCourse.php?id=${id}`;
                }
            })

        }
    </script> -->
</body>

</html>