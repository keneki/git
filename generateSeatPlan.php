<?php
include 'model/dbhelper.php';

if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}

$schedId = $_GET['schedId'];
$getSchedDates = getSchedDates($schedId);
$getSched = getSchedById($schedId);
$getStud = getStudentsPerSched1($schedId);
$getProf = getProfName($schedId);
$courseList = getSchedProf($_SESSION['id']);
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>

</head>
<style type='text/css'>
    .foo {
        float: left;
        width: 75px;
        height: 75px;
        margin: 5px;
        border: 3px solid rgba(0, 0, 0, .2);
        text-align: center;

    }

    .blue {
        background: #13b4ff;
    }

    .purple {
        background: #ab3fdd;
    }

    .wine {
        background: #ae163e;
    }
</style>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <div class="section card-panel" style="margin-bottom: 24px">
                <h4 class="center-align light">Course Details</h4>
                <div class="row">
                    <div class="col s4">
                        <h6>Class Course: <?php echo $getSched["course_code"] ?></h6>
                        <h6>Room Name: <?php echo $getSched["room"] ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Schedule: <?php echo $getSched["time_start"] . '-' . $getSched["time_end"] . ' ' . $getSched['sched']   ?></h6>
                        <h6>Room Type: <?php echo $getSched["room_type"] ?  "Laboratory" : "Classroom"  ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Group No: <?php echo $getSched["group_no"] ?></h6>
                        <h6>Instructor: <?php echo $getProf["profFirstname"] . ' ' . $getProf["profLastname"] ?></h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <table id="example" class="mdl-data-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID Number</th>
                                <th>Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($getStud as $data) {

                            ?>
                                <tr>

                                    <td><?php echo $data['userIdNo'] ?></td>
                                    <td><?php echo $data['firstName'] . ' ' . $data['lastName'] ?></td>


                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

                <div class="col s8">

                    <form method="POST">
                        <div class="row" style="padding: 1em">
                            <div class="input-field col s6">
                                <select required="required" id="row_column_id" name="generate">
                                    <option value="" disabled selected>Choose your option</option>
                                    <option value="automatic" name="automatic">Automatic</option>
                                    <option value="manual" name="manual">Manual</option>
                                </select>
                                <label>Type of Generator</label>
                            </div>
                        </div>
                        <div id="row_column">
                            <div class="row" style="padding: 1em">
                                <div class="input-field col s6">
                                    <select id="row_id" name="rows">
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                    <label>Row </label>
                                </div>
                                <div class="input-field col s6">
                                    <select id="column_id" name="columns">
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                    <label>Column </label>
                                </div>
                            </div>
                        </div>
                        <input type="submit" id="submitForm" name="submitForm" value="Generate">
                    </form>



                </div>


            </div>

            <table width=50% height=50% border=2px>
                <?php


                if (isset($_POST["submitForm"]) && isset($_POST['rows']) && isset($_POST['columns'])) {
                    $rows = $_POST['rows'];
                    $columns = $_POST['columns'];
                    $generate = $_POST['generate'];
                    $total_enrolled = 2;
                    $startid = 1;
                    $product = $rows * $columns;
                    if ($generate == 'manual' && $product <=45) {
                        echo "<div class = 'row'> <div class = 'col s".$columns."offset-s5'>";
                        for ($i = 0; $i < $rows; $i++) {
                            echo "<div>";
                            for ($j = 0; $j < $columns; $j++)
                                echo "  <div class = 'foo align-center tooltipped' data-toggle = 'popover-hover' data-position= 'top'>" . $data['firstName'] . "</div>";

                            echo "</div>";
                        }

                        echo "</div></div>";
                    }
                } elseif (isset($_POST["submitForm"])) {
                    $generate = $_POST['generate'];
                    if ($generate == 'automatic') {
                        $CEAC = 1;
                        $LB264 = 0;
                        if ($CEAC == 1) {
                            $maxcols = 5;
                            $maxid = 20;
                            $startid = 1;
                            echo "<div class = 'row'> <div class = 'col s5 offset-s5'>";
                            for ($i = 1; $i <= ceil($maxid / $maxcols); $i++) {

                                echo "<div>";
                                for ($j = 1; $j <= $maxcols; $j++)
                                    if ($startid <= $maxid)
                                        echo "  <div class = 'foo align-center tooltipped' data-toggle = 'popover-hover' data-position= 'top'>" . $startid++ . "</div>";

                                    else
                                        echo "</div>";
                            }
                            echo "</div></div>";
                        } elseif ($LB264 == 1) {
                            $maxcols = 7;
                            $maxid = 40;
                            $startid = 1;
                            echo "<div class = 'row'> <div class = 'col s".$maxcols." offset-s4'>";
                            for ($i = 1; $i <= ceil($maxid / $maxcols); $i++) {

                                echo "<div>";
                                for ($j = 1; $j <= $maxcols; $j++)
                                    if ($startid <= $maxid)
                                        foreach($getStud as $data){
                                            echo "  <div class = 'foo align-center tooltipped' data-toggle = 'popover-hover' data-position= 'top'>" . $startid++ . "</div>";
                                        }
                                        

                                    else
                                        echo "</div>";
                            }
                            echo "</div></div>";
                        }
                    }
                }

                ?>
            </table>
        </div>

        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, {
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#row_column_id").on("change", function() {
                var x = $(this).val();
                if (x == "automatic") {
                    $("#row_column").closest("div").hide();
                } else {
                    $("#row_column").closest("div").show();
                }
            });


        });


        $('.tooltipped').tooltip({
            html: " <img src=<?php echo 'images/user/' . $data['userIdNo'] . '.png' ?>>"
        });
    </script>
</body>

</html>