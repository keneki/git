<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <!-- additional css -->
    <link type="text/css" rel="stylesheet" href="css/sweetalert2.min.css" media="screen,projection" />
    <!-- Custom Css -->
    <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" />
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Attendance Monitoring System</title>
</head>

<body>
    <div class="container">
        <div class="section card-panel z-depth-4"
            style="border-radius: 20px;margin-top: 10%;padding: 50px 75px;">
            <div class="container">
                
            </div>
            <form action="controller/newpassforgot.php" method="POST">
            
                <div class="input-field col s12">
                    <input id="username" type="text" name="username" value="<?php echo $_GET['username']?>" hidden>
                    <input id="userId" type="number" name="userId" value="<?php echo $_GET['id']?>" hidden>
                    <input id="newpassword" type="password" name="newpassword">
                    <label class="active" for="newpassword">New Password</label>
                    
                </div>
                <div class="input-field col s12">
                    <input id="cnewpassword" type="password" name="cnewpassword">
                    <label class="active" for="cnewpassword">Confirm New Password</label>
                </div>
                
                    <button id="btnNewPassword" name="btnNewPassword" class="btn waves-effect waves-light "
                         type="submit" name="action">Done
                        <i class="material-icons right">send</i>
                    </button>
            </form>
           
    </div>
</div>


    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <!-- custom js -->

</body>

</html>