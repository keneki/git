<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
$userList=getAllUsers();
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'head.php'?>
</head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

<main>
       
<div class="section" style="margin: 2rem;">
    <form action="controller/accountCtrl.php" method="POST">
    <div class="row">
        <h4>ADD AN ACCOUNT</h4>
        <p>Fill up all fields.</p>
    
        <label for="userName">Name</label>
        <input id="userName" name="userName" type="text" class="validate">
        
        

        <label for="userUsername">Username</label>
        <input id="userUsername" type="text" name="userUsername" class="validate">
        
        
    
        <label for="userPassword">Password</label>
        <input id="userPassword" type="password" name="userPassword" class="validate">
        
        

        <label for="userType">User Number</label>
        <input id="userType" type="text" pattern="[0-9]+" name="userType" class="validate">
        
        
    
    <button id="userbtn" type="submit" class="btn waves-effect waves-light" name="userbtn">Add User</button>
    </div>
    </form>
</div>




</main>
<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
</body>

</html>