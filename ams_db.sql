-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2020 at 02:29 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_uid` int(11) NOT NULL,
  `class_code` varchar(255) NOT NULL,
  `class_section` varchar(255) NOT NULL,
  `class_description` varchar(255) NOT NULL,
  `class_unit` double NOT NULL,
  `class_start` time NOT NULL,
  `class_end` time NOT NULL,
  `class_sched` varchar(255) NOT NULL,
  `class_roomNumber` varchar(10) NOT NULL,
  `prof_uid` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_uid`, `class_code`, `class_section`, `class_description`, `class_unit`, `class_start`, `class_end`, `class_sched`, `class_roomNumber`, `prof_uid`, `dateCreated`) VALUES
(2, 'sad', 'sad', 'sad', 1, '00:00:00', '01:00:00', 'ads', '123', 9, '2019-08-12 16:00:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_uid` int(11) NOT NULL,
  `course_code` varchar(255) NOT NULL,
  `desc_title` varchar(255) NOT NULL,
  `group_no` varchar(255) NOT NULL,
  `units` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_uid`, `course_code`, `desc_title`, `group_no`, `units`, `dateCreated`) VALUES
(1, 'CPE 76N', 'Mobile Application Development', 'G1', 3, '2020-01-25 07:55:25'),
(2, 'CPE 422N', 'Operating Systems', 'G1', 3, '2020-01-26 12:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept`
--

CREATE TABLE `tbl_dept` (
  `dept_name` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deptId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dept`
--

INSERT INTO `tbl_dept` (`dept_name`, `dateCreated`, `deptId`) VALUES
('Electronics Engineering', '2019-09-02 13:07:42', 1),
('Electrical Engineering', '2019-09-02 13:07:32', 2),
('Civil Engineering', '2019-09-02 14:09:37', 3),
('Mechanical Engineering', '2019-09-02 15:10:37', 4),
('Computer Engineering\r\n', '2020-01-25 06:40:57', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_logs`
--

CREATE TABLE `tbl_logs` (
  `logs_id` int(11) NOT NULL,
  `sched_uid` int(11) NOT NULL,
  `time_in` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `time_out` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL,
  `logs_status` varchar(255) NOT NULL,
  `logs_statuss` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prof`
--

CREATE TABLE `tbl_prof` (
  `prof_uid` int(11) NOT NULL,
  `profFirstname` varchar(255) NOT NULL,
  `profLastname` varchar(255) NOT NULL,
  `profCardId` double NOT NULL,
  `profIdNum` int(255) NOT NULL,
  `deptId` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_prof`
--

INSERT INTO `tbl_prof` (`prof_uid`, `profFirstname`, `profLastname`, `profCardId`, `profIdNum`, `deptId`, `dateCreated`) VALUES
(1, 'James', 'Labrador', 123456, 324, 5, '2020-01-29 08:31:37'),
(15, 'Marvin', 'Radaza', 4231658, 789, 2, '2020-01-25 06:42:19'),
(16, 'Joselito', 'Valdezamo', 15387467, 123, 3, '2020-01-25 06:42:19'),
(17, 'Allan', 'Pangan', 15387468, 321, 4, '2020-01-25 06:42:19'),
(18, 'Victoria', 'Yap', 15347865, 456, 5, '2020-01-25 06:42:19'),
(19, 'Marlito', 'Patunob', 5412396, 654, 5, '2020-01-25 06:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `sched_uid` int(11) NOT NULL,
  `course_uid` int(11) NOT NULL,
  `prof_uid` int(11) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `sched` varchar(255) NOT NULL,
  `room` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `room_type` tinyint(1) NOT NULL,
  `total_enrolled` int(11) NOT NULL,
  `timeAllowance` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schedule`
--

INSERT INTO `tbl_schedule` (`sched_uid`, `course_uid`, `prof_uid`, `time_start`, `time_end`, `sched`, `room`, `dateCreated`, `room_type`, `total_enrolled`, `timeAllowance`) VALUES
(1, 1, 1, '07:30:00', '10:30:00', 'F', 'CEAC3', '2020-01-29 04:42:02', 1, 50, '15'),
(2, 2, 1, '08:30:00', '10:00:00', 'MW', 'LB264TC', '2020-01-29 08:41:05', 0, 45, '15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `userUsername` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `userType` int(11) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `userIdNo` varchar(255) NOT NULL,
  `deptId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `userName`, `userUsername`, `userPassword`, `userType`, `lastName`, `firstName`, `userIdNo`, `deptId`) VALUES
(1, 'Teacher', '1', '1', 0, 'Fabian', 'Elline', '00001', 5),
(2, 'Student', '15104094', '3', 1, 'Espinosa', 'Jelou', '00002', 2),
(3, 'Student', '15104400', '2', 1, 'Flores', 'Kenneth', '15104400', 3),
(4, 'Student', '15104399', '1', 1, 'Flores', 'Kenneth Kent', '15104399', 5),
(5, 'Teacher', 'teacher', 'teacher', 0, 'Pooh', 'Winnie', '00005', 5),
(6, 'Chairperson', 'chairperson', 'chairperson', 2, 'Stinson', 'Barney', '00006', 2),
(7, 'Student', '15103001', '4', 1, 'Dalin', 'John Roy', '15103001', 5),
(8, 'Teacher', 'Van', '1', 0, 'Patiluna', 'Van', '1111111', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_uid`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_uid`);

--
-- Indexes for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  ADD PRIMARY KEY (`deptId`);

--
-- Indexes for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  ADD PRIMARY KEY (`logs_id`);

--
-- Indexes for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  ADD PRIMARY KEY (`prof_uid`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`sched_uid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  MODIFY `deptId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_logs`
--
ALTER TABLE `tbl_logs`
  MODIFY `logs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  MODIFY `prof_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  MODIFY `sched_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
