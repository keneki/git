<?php
include '../model/dbhelper.php';
if(isset($_POST['update'])){
    $prof_uid= htmlentities($_POST['prof_uid']);
    $profFirstname= htmlentities($_POST['profFirstname']);
    $profLastname= htmlentities($_POST['profLastname']);
    //$profList= ($_POST['prof_list']);
    $profCardId= htmlentities($_POST['profCardId']);
    $profIdNum=htmlentities($_POST['profIdNum']);
    $dept_name=htmlentities($_POST['dept_name']);
    $prof=array();

    // foreach($profList as $selected) {
    //     array_push($prof,$selected);
    // }
    //     // print_r($schedule);
    //     $prof= implode(",",$prof);
     

    $data=array($profFirstname,$profLastname,$profCardId,$profIdNum,$dept_name,$prof_uid);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    
    if($flag){
        updateInstructor($data);
        header("Location:../instructor.php?status=success_updating");
    }
    else{
        echo "<script> alert('Error Updating') </script>";
        header("Location:../instructor.php?status=failed_updating");
    }

}