<?php
include '../model/dbhelper.php';


if(isset($_POST['btnAddProf'])){
    $profFirstname= htmlentities($_POST['profFirstname']);
    $profLastname= htmlentities($_POST['profLastname']);
    $profCardId= htmlentities($_POST['profCardId']);
    $profIdNum= htmlentities($_POST['profIdNum']);
    $dept_name= htmlentities($_POST['dept_name']);

    $data=array($profFirstname,$profLastname,$profCardId,$profIdNum,$dept_name);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    if($flag){
        addProf($data);
        header("Location:../instructor.php?status=successInstructor");
    }
    else{
        echo "<script> alert('Error Adding') </script>";
        header("Location:../instructor.php?status=failedInstructor");
    }

}