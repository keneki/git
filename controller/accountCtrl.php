<?php
include '../model/dbhelper.php';


if(isset($_POST['userbtn'])){

    $userName= htmlentities($_POST['userName']);
    $userUsername= htmlentities($_POST['userUsername']);
    $userPassword= htmlentities($_POST['userPassword']);
    $userType= htmlentities($_POST['userType']);


    $data=array($userName,$userUsername,$userPassword,$userType);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    if($flag){
        addAccount($data);
        header("Location:../dashboard.php?status=successAddUser");
    }
    else{
        echo "<script> alert('Error Adding') </script>";
        header("Location:../dashboard.php?status=failedAddUser");
    }

}