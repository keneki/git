<?php
include '../model/dbhelper.php';


if(isset($_POST['btnAddCourse'])){
    $subject_code= htmlentities($_POST['subject_code']);
    $desc_title= htmlentities($_POST['desc_title']);
    $section= htmlentities($_POST['section']);
    $units= htmlentities($_POST['units']);


    $data=array($subject_code,$desc_title,$section,$units);
    $flag=true;

    foreach($data as $d){
        if(empty($d)){
            $flag=false;
            break;
        }
    }

    
    if($flag){
        addCourse($data);
        header("Location:../course.php?status=success");
    }
    else{
        echo "<script> alert('Error Adding') </script>";
    }
    header("Location:../course.php?status=failed");

}