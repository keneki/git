<?php
include '../model/dbhelper.php';
if (isset($_POST['btnUpdateStatus'])) {
    $schedId = $_POST['btnUpdateStatus'];
    foreach ($_POST as $key => $value) {
        $id = explode(",", $key);
        if (count($id) > 1) {
            $attendanceId = $id[1];
            $status = $value;
            $data = array($status, $attendanceId);
            $result = updateStatus($data);
            if ($result) {
                header('location:../viewSched.php?schedId=' . $schedId . '');
            }
            else {
                echo $result;
            }

        }
    }
}
