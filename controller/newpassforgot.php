<?php
include '../model/dbhelper.php';

if(isset($_POST['btnNewPassword'])){
    //$oldpass = $_POST['oldpassword'];
    $cnewpass = $_POST['cnewpassword'];
    $newpass = $_POST['newpassword'];
    $id =$_POST['userId'];
    $username = $_POST['username'];
    $ok1=getEmail($_POST['username']);
    if($newpass == $cnewpass){
        $data = array($newpass,$id);
    //$result = checkOldPassword(array($id, $oldpass));
    if(0 < $data){
        
        $ok = updatePassword($data);

        if($ok){
            $currentUser = login($username,$newpass);

            $_SESSION['id'] = $currentUser['userId'];
            $_SESSION['userName'] = $currentUser['userName'];
            $_SESSION['userType'] = $currentUser['userType'];
            
            header('location:../dashboard.php?status=passwordUpdated');

        }else{
            echo 'error';
        }
    }else{
        header('location:../index.php?id='.$id.'&username='.$username.'&status=Newpass_success'); 
    }
    }
    else{
        header('location:../forgotpassword.php?InvalidPassword');
    }
    
// print_r($data);
// echo $username;
// die;
}
elseif(isset($_POST['btnSettingsNewPassword'])){
    //$oldpass = $_POST['oldpassword'];

    $cnewpass = $_POST['cnewpassword'];
    $newpass = $_POST['newpassword'];
    $id =$_POST['userId'];
    $username = $_POST['username'];
    //$ok1=getEmail($_POST['username']);
    $passLength = strlen($newpass);
    if( $passLength >=8 && $passLength <= 15){
        if($newpass == $cnewpass){

            $data = array($newpass,$id);
        //$result = checkOldPassword(array($id, $oldpass));
        if(0 < $data){
            
            $ok = updatePassword($data);
    
            if($ok){
            $currentUser = login($username,$newpass);
    
                $_SESSION['id'] = $currentUser['userId'];
                $_SESSION['userName'] = $currentUser['userName'];
                $_SESSION['userType'] = $currentUser['userType'];
                
                header('location:../dashboard.php?status=passwordUpdated');
    
            }else{
                echo 'error';
            }
        }else{
            header('location:../index.php?id='.$id.'&username='.$username.'&status=Newpass_success'); 
        }
        }
        else{
            header('location:../settings.php?status=InvalidPassword');
        }
    }
    else{
        echo 'Password should be at least 8 characters in length and maximum of 15 characters!';
    }
    
    
// print_r($data);
// echo $username;
// die;
}
?>