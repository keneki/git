<?php
include '../model/dbhelper.php';
if (isset($_POST['updateTime'])) {
    $time_start = $_POST['time_start'];
    $time_end = $_POST['time_end'];
    $allowance = $_POST['allowance'];
    $attDayId = $_POST['updateTime'];
    $schedId = $_POST['schedId'];



    $data = array($time_start, $time_end, $allowance, $attDayId);
    $result = updateSchedTime($data);

    if ($result) {
        header('location:../viewSched.php?schedId='. $schedId);
    }
}
