<?php
include '../model/dbhelper.php';

if(isset($_POST['btnChangePassword'])){
    $oldpass = $_POST['oldpassword'];
    $newpass = $_POST['newpassword'];
    $id =$_POST['userId'];
    $username = $_POST['username'];
    $data = array($newpass,$id);
    $result = checkOldPassword(array($id, $oldpass));
    if(0 < $result){
        $ok = updatePassword($data);

        if($ok){
            $currentUser = login($username,$newpass);

            $_SESSION['id'] = $currentUser['userId'];
            $_SESSION['userName'] = $currentUser['userName'];
            $_SESSION['userType'] = $currentUser['userType'];
            
            header('location:../dashboard.php?status=passwordUpdated');

        }else{
            echo 'error';
        }
    }else{
        header('location:../enterNewPassword.php?id='.$id.'&username='.$username.'&status=passwordincorrect'); 
    }
// print_r($data);
// echo $username;
// die;
}