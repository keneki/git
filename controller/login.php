<?php

include  '../model/dbhelper.php';


$username=$_POST['email'];
$password=$_POST['password'];

$currentUser = login($username, $password);

if((!empty($username) && $username==$currentUser["userUsername"])&&(!empty($password) && $password==$currentUser["userPassword"])){
    $_SESSION['loggedin'] = TRUE;
    $_SESSION['id'] = $currentUser['userId'];
    $_SESSION['userIdNo'] = $currentUser['userIdNo'];
    $_SESSION['userType'] = $currentUser['userType'];
    $_SESSION['firstName'] = $currentUser['firstName'];
    $_SESSION['lastName'] = $currentUser['lastName'];
    $_SESSION['deptName'] = $currentUser['dept_name'];
    
    if ($_SESSION['userType'] == 0){
        header("location:../dashboard.php");
    }
    elseif ($_SESSION['userType'] == 1){
        header("location:../dashboardStudent.php");
    }
    elseif ($_SESSION['userType'] == 2){
        header("location:../dashboardChair.php");
    }
    
}
else{
    header("location:../index.php?failed_login");
}