<header>

    <nav>
        <div class="nav-wrapper  white navbar-fixed">
            <div class="row">
                <div class="col s12">
                    <a id="burger-icon-1" href="#" data-target="sidebar-1" class="left sidenav-trigger show-on-medium grey-text text-darken-2"><i class="material-icons">menu</i></a>
                    <a href="#" class="brand-logo grey-text text-darken-2" style="left:15px"><i class="material-icons">home</i>Attendance Monitoring System</a>
                </div>
            </div>
        </div>
    </nav>


    <!-- SIDEBAR 1 - HAS .SIDENAV-FIXED -->
    <ul id="sidebar-1" class="sidenav sidenav-fixed">
        <li>
            <!-- Put the user data here -->
            <div class="user-view center-align">
                <h6 class="white-text" style="margin-bottom:24px">Department of <?php echo $_SESSION['deptName'] ?></h6>
                <img class="circle userImage" src=<?php echo 'images/user/' . $_SESSION['userIdNo'] . '.png' ?>>
                <h6 class=" white-text">ID No.: <?php echo $_SESSION['userIdNo'] ?></h6>
                <h6 class=" white-text"><?php echo $_SESSION['firstName'] . ' ' . $_SESSION['lastName'] ?></h6>
            </div>
            <!-- Put the user data here Eof-->
        </li>
        <?php
        if ($_SESSION['userType'] == 0) {     //teacher
            echo "
                <li><a href='dashboard.php?status='><i class='material-icons'>home</i> Dashboard</a></li>
                <li><a href='seatPlan.php?status='><i class='material-icons'>build</i> Seat Plan</a></li>
                <li><a href='settings.php?status='><i class='material-icons'>settings</i> Settings</a></li>
                <!---<li><a href='course.php?status='><i class='material-icons'>domain</i> Course</a></li>
                <li><a href='instructor.php?status='><i class='material-icons'>person_outline</i> Instructors</a></li>
                <li><a href='course.php?status='><i class='material-icons'>domain</i> Course</a></li>
                <li><a href='schedule.php?status='><i class='material-icons'>event</i> Schedule</a></li>
                <li><a href='addAccount.php?status='><i class='material-icons'>group_add</i> Add Account</a></li>
                <li><a href='changePassword.php?status='><i class='material-icons'>edit_attributes</i> Change Password</a></li> -->
                ";
        } else if ($_SESSION['userType'] == 1) {    //student
            echo "<li ><a href='dashboardStudent.php?status='><i class='material-icons'>home</i>Dashboard</a></li>
                <!--<li><a href='hrstaff_ins.php?status='><i class='material-icons'>person_outline</i> Instructors</a></li>
                <li><a href='hrstaff_course.php?status='><i class='material-icons'>domain</i> Course</a></li>
                <li><a href='hrstaff_sched.php?status='><i class='material-icons'>event</i> Schedule</a></li> -->
                <li><a href='settings.php?status='><i class='material-icons'>settings</i> Settings</a></li>
                ";
        } else {   //chairperson
            echo "<li ><a href='dashboardChair.php?status='><i class='material-icons'>home</i>Dashboard</a></li>
                <!---<li><a href='nas_sched.php?status='><i class='material-icons'>event</i> Schedule</a></li> -->
                <li><a href='settings.php?status='><i class='material-icons'>settings</i> Settings</a></li>";
        }


        ?>
        <li><a href="controller/logout.php?status="><i class="material-icons">logout</i>Sign Out</a></li>
    </ul>
</header>