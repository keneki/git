<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}

 $courseList=getAllCourse();
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
        <form class="form-horizontal" action="" method="post" name="uploadCSV"
            enctype="multipart/form-data">
        <div class="input-row">
        <a class="col-md-4 control-label">Choose CSV File</a> <input
            type="file" name="file" id="file" accept=".csv">
        <button type="submit" class="btn btn-primary" id="submit" name="import"
            class="btn-submit">Import</button>
        <br />

        </div>
        <div id="labelError"></div>
        </form>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Course</a>
        </div>
        <div class="section" style="margin: 2rem;">
        <?php
        $conn = mysqli_connect("localhost", "root", "", "ams_db");

        if (isset($_POST["import"])) {
    
        $fileName = $_FILES["file"]["tmp_name"];
    
         if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $sqlInsert = "INSERT into tbl_course (course_uid,course_code,desc_title,group_no,units)
                   values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $column[3] . "','" . $column[4] ."')";
            $result = mysqli_query($conn, $sqlInsert);
            
            if (! empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
        }
    }
}
?>

<?php
$sqlSelect = "SELECT * FROM tbl_course";
$result = mysqli_query($conn, $sqlSelect);
            
if (mysqli_num_rows($result) > 0) {
?>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <!-- concat intructor name -->
                        <th>Subject Code</th>
                        <th>Descriptive Title</th>
                        <th>Section</th>
                        <th>Units</th>
                        <!-- concat time start and end -->
                        <th>Action</th>
                    </tr>
                </thead>
                <?php
	                while ($courseList = mysqli_fetch_array($result)) {
                ?>
                <tbody>
                    
                    <tr>
                        <td><?php echo $courseList['course_code']?></td>
                        <td><?php echo $courseList['desc_title']?></td>
                        <td><?php echo $courseList['group_no']?></td>
                        <td><?php echo $courseList['units']?></td>
                        <td>
                            <button onclick="test(<?php echo $courseList['course_uid']?>)"
                                class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
                        </td>
                    </tr>
                    <?php
                        }
                    ?>
                    <?php
                    }
                    ?>
                </tbody>
            </table>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Course</h4>
            <div class="row">
                <form action="controller/courseCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <input id="subject_code" name="subject_code" type="text" pattern="[a-zA-Z0-9\s]+" class="validate">
                        <label for="subject_code">Course Code</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="desc_title" name="desc_title" type="text" pattern="^[A-Za-z0-9 ,/]+$" class="validate">
                        <label for="desc_title">Descriptive Title</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="section" name="section" type="text" pattern="[a-zA-Z0-9]+" class="validate">
                        <label for="section">Group No.</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="units" name="units" type="text" pattern="[0-9]+" title="Please input number only" class="validate">
                        <label for="units">Units</label>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddCourse">Add
                Course
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3]
                    //     }
                    // },

                    {
                        text: 'Export to PDF',
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script> -->
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `controller/deleteCourse.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>