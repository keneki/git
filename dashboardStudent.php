<?php
include 'model/dbhelper.php';

if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}
$courseList = getSchedStudents($_SESSION['id']);

?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>

</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <table id="example" class="mdl-data-table" style="width:100%">
                <thead>
                    <tr>
                        <th>Course Code</th>
                        <th>Time</th>
                        <th>Room</th>
                        <th>Options</th>
                        <!-- concat time start and end and days-->
                        <!-- <th>Time</th>
                        <th>Day/s</th>
                        <th>Room</th>
                        <th>Time in</th>
                        <th>Time out</th>
                        <th>Time in Status</th>
                        <th>Time out Status</th> -->
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($courseList as $logsData) {
                    ?>
                        <tr>
                            <td><?php echo $logsData['course_code'] . ' - ' . $logsData['group_no'] . '  ' . $logsData['room'] ?></td>
                            <!-- sql query concat lanme and fname -->
                            <td><?php echo $logsData['time_start'] . ' - ' . $logsData['time_end'] . '  ' . $logsData['sched'] ?></td>
                            <td><?php echo $logsData['room']  ?></td>
                            <td>
                                <div class="section" style="margin: -1rem;">
                                    <!-- Modal Trigger -->
                                    <a class="waves-effect waves-light btn" href="viewStudent.php?schedId=<?php echo $logsData['sched_uid'] ?>">View</a>
                                </div>
                                <!-- modal -->
                                <!-- Modal Structure -->
                                <div id="modal1" class="modal">
                                    <div class="section" style="margin: 2rem;">
                                        <div>
                                            <h4>Course</h4>
                                            <table id="example" class="display" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Course Code</th>
                                                        <th>Time</th>
                                                        <!-- concat time start and end and days-->
                                                        <!-- <th>Time</th>
                                                        <th>Day/s</th>
                                                        <th>Room</th>
                                                        <th>Time in</th>
                                                        <th>Time out</th>
                                                        <th>Time in Status</th>
                                                        <th>Time out Status</th> -->
                                                        <!-- <th>Action</th> -->
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($courseList as $logsData) {
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $logsData['course_code'] . ' - ' . $logsData['group_no'] . '  ' . $logsData['room'] ?></td>
                                                            <!-- sql query concat lanme and fname -->
                                                            <td><?php echo $logsData['time_start'] . ' - ' . $logsData['time_end'] . '  ' . $logsData['sched'] ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>

                                        </div>


                                    </div>
                                </div>
                                <!-- modal end -->
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>

        </div>

        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, {
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     text: 'Export to PDF',
                    //     extend: 'pdf',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // }
                ]
            });
        });
    </script>
</body>

</html>