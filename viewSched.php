<?php
include 'model/dbhelper.php';

if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}

$schedId = $_GET['schedId'];
$getSchedDates = getSchedDates($schedId);
$getSched = getSchedById($schedId);
$getProf = getProfName($schedId);
$getStudentPerClass = getStudentsPerClass($schedId);
?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>

</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <div class="section card-panel" style="margin-bottom: 24px">
                <h4 class="center-align light">Course Details</h4>
                <div class="row">
                    <div class="col s4">
                        <h6>Class Course: <?php echo $getSched["course_code"] ?></h6>
                        <h6>Room Name: <?php echo $getSched["room"] ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Schedule: <?php echo $getSched["time_start"] . '-' . $getSched["time_end"] . ' ' . $getSched['sched']   ?></h6>
                        <h6>Room Type: <?php echo $getSched["room_type"] ?  "Laboratory" : "Classroom"  ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Group No: <?php echo $getSched["group_no"] ?></h6>
                        <h6>Instructor: <?php echo $getProf["profFirstname"] . ' ' . $getProf["profLastname"] ?></h6>
                    </div>
                </div>
            </div>

            <table id="example" class="mdl-data-table" style="width:100%;margin-bottom: 5em">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Attendance</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($getSchedDates as $data) {
                        $attendees = getAttendeesCount($data['date'], $data['sched_uid'])['count'];
                    ?>
                        <tr>
                            <td><?php echo $data['date'] ?></td>
                            <td><?php echo  $attendees . ' / ' . $data['total_enrolled']  ?></td>
                            <td>
                                <div class="section">
                                    <!-- Modal Trigger -->
                                    <a class="waves-effect waves-light btn modal-trigger" style="width: 50px;background-color: grey " href="#modal<?php echo $data['date'] ?>">View</a>
                                    <a class="waves-effect waves-light btn modal-trigger" href="#modalEdit<?php echo $data['date'] ?>" style="width: 50px;">Update</a>

                                </div>
                                <!-- modal -->
                                <!-- View Modal -->
                                <div id="modal<?php echo $data['date']  ?>" class="modal">
                                    <form action="controller/editStudentStatus.php" method="POST">
                                        <div class="modal-content" style="margin: 2rem;">
                                            <h4 class="center-align">Attendance</h4>
                                            <table id="example" class="mdl-data-table" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <td>ID No.</td>
                                                        <td>Name</td>
                                                        <td>Status</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach (getStudentsPerSched($data['att_day_id']) as $userData) {
                                                        $studentCreds = getStudent($userData['userId']);
                                                        $status = getStatus($userData['status']);
                                                    ?>
                                                        <tr>
                                                            <td><?php echo $studentCreds['userIdNo'] ?></td>
                                                            <td><?php echo $studentCreds['firstName'] . ' ' . $studentCreds['lastName'] ?></td>
                                                            <td>

                                                                <select id='select,<?php echo $userData['attendId'] ?>' name='select,<?php echo $userData['attendId'] ?>' type="select" class="browser-default">

                                                                    <option value="1" <?php if ($status['statusId'] == "1") {
                                                                                            echo 'selected';
                                                                                        } ?>>PRESENT</option>
                                                                    <option value="2" <?php if ($status['statusId'] == "2") {
                                                                                            echo 'selected';
                                                                                        } ?>>LATE</option>
                                                                    <option value="3" <?php if ($status['statusId'] == "3") {
                                                                                            echo 'selected';
                                                                                        } ?>>ABSENT </option>
                                                                    <option value="4" <?php if ($status['statusId'] == "4") {
                                                                                            echo 'selected';
                                                                                        } ?>>EXCUSE</option>

                                                                </select>

                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button id="button" type="submit" name="btnUpdateStatus" value=<?php echo $schedId ?> class="modal-action modal-close waves-effect waves-green btn-flat">SAVE</button>
                                            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " style="margin-right: 4em">CLOSE</a>
                                        </div>
                                    </form>
                                </div>
                                <!-- modal end -->


                                <!-- Edit Modal -->
                                <div id="modalEdit<?php echo $data['date'] ?>" class="modal">
                                    <form action="controller/editAllowance.php" method="POST">
                                        <div class="modal-content" style="margin: 2rem;">
                                            <h4 class="center-align">Edit Time IN/OUT/ALLOWANCE</h4>
                                            <div class="row" style="padding: 1em">
                                                <div class="input-field col s6">
                                                    <input id="time_start" class="timepicker" name="time_start" type="text" class="validate" value=<?php echo  date($data['time_start_day']) ?>>
                                                    <label for="time_start">Time Start</label>
                                                </div>
                                                <div class="input-field col s6">
                                                    <input id="time_end" class="timepicker" name="time_end" type="text" class="validate" value=<?php echo $data['time_end_day'] ?>>
                                                    <label for="time_end">Time End</label>
                                                </div>
                                            </div>
                                            <div class="row" style="padding: 1em">
                                                <div class="input-field col s12">
                                                    <?php $time_allowance = $data['time_allowance']
                                                    ?>
                                                    <select name="allowance" id="allowance">
                                                        <option value="00:00:00" <?php if ($time_allowance == "00:00:00") {
                                                                                        echo "selected";
                                                                                    } ?>>None</option>
                                                        <option value="00:05:00" <?php if ($time_allowance == "00:05:00") {
                                                                                        echo "selected";
                                                                                    } ?>>5 minutes</option>
                                                        <option value="00:10:00" <?php if ($time_allowance == "00:10:00") {
                                                                                        echo "selected";
                                                                                    } ?>>10 minutes</option>
                                                        <option value="00:15:00" <?php if ($time_allowance == "00:15:00") {
                                                                                        echo "selected";
                                                                                    } ?>>15 minutes</option>
                                                        <option value="00:20:00" <?php if ($time_allowance == "00:20:00") {
                                                                                        echo "selected";
                                                                                    } ?>>20 minutes</option>
                                                        <option value="00:25:00" <?php if ($time_allowance == "00:25:00") {
                                                                                        echo "selected";
                                                                                    } ?>>25 minutes</option>
                                                    </select>
                                                    <label>Allowance</label>
                                                    <div class="input-field col s3">
                                                        <button class="btn" type="submit" value="<?php echo $data['att_day_id'] . ',' . $schedId ?>" name="updateTime">SAVE</button>
                                                        <input type="hidden" name="schedId" value="<?php echo $schedId ?>">
                                                    </div>
                                                </div>
                                    </form>
                                </div>
        </div>
        <div class="modal-footer">


        </div>
        </div>
        </td>

        </tr>
    <?php
                    }
    ?>
    </tbody>
    </table>

    <table id="example" class="mdl-data-table" style="width:100%">
        <thead>
            <tr>
                <td>ID No.</td>
                <td>Name</td>
                <td>Absences</td>
                <td>Tardiness</td>
                <td>Excused</td>
            </tr>

        </thead>
        <tbody>
            <?php
            foreach ($getStudentPerClass as $student) {
                $status = getStudentStatus($student['userId']);
                $absences = [];
                $tardiness = [];
                $excused = [];
                foreach ($status as $result) {
                    if ($result['MIN'] > $getSched["time_start"] || null == $result) {
                        array_push($absences, $result['MIN']);
                    }
                }

            ?>
                <tr>
                    <td><?php echo $student['userIdNo'] ?></td>
                    <td><?php echo $student['firstName'] . " " . $student['lastName'] ?></td>
                    <td><?php echo count($absences) ?></td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
    </div>

    <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, {
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },
                ],
            }, );
        });
    </script>
</body>

</html>