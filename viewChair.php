<?php
include 'model/dbhelper.php';

if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}

$schedId = $_GET['schedId'];
$getProf = getProfName($schedId);
$getSchedDates = getSchedDates($schedId);
$getSched = getSchedById($schedId);

?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>

</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <div class="section card-panel" style="margin-bottom: 24px">
                <h4 class="center-align light">Course Details</h4>
                <div class="row">
                    <div class="col s4">
                        <h6>Class Course: <?php echo $getSched["course_code"] ?></h6>
                        <h6>Room Name: <?php echo $getSched["room"] ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Schedule: <?php echo $getSched["time_start"] . '-' . $getSched["time_end"] . ' ' . $getSched['sched']   ?></h6>
                        <h6>Room Type: <?php echo $getSched["room_type"] ?  "Laboratory" : "Classroom"  ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Group No: <?php echo $getSched["group_no"] ?></h6>
                        <h6>Instructor: <?php echo $getProf["profFirstname"] . ' ' . $getProf["profLastname"] ?></h6>
                    </div>
                </div>
            </div>

            <table align = "center" class="mdl-data-table" style="width:50%">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($getSchedDates as $data) {
                        $attendees = getAttendeesCount($data['date'],$data['sched_uid'])['count'];
                    ?>
                        <tr>
                            <td><?php echo $data['date'] ?></td>

                            <td>
                                <div class="section">
                                    <!-- Modal Trigger -->
                                    <a class="waves-effect waves-light btn modal-trigger"  href="#modal<?php echo $data['date'] ?>">View</a>
                                </div>
                                <!-- modal -->
                                <!-- View Modal -->
                                <div id="modal<?php echo $data['date']  ?>" class="modal">
                                    <div class="modal-content" style="margin: 2rem;">
                                        <h4 class="center-align">Attendance</h4>
                                        <table id="example" class="mdl-data-table" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td>ID No.</td>
                                                    <td>Name</td>
                                                    <td>Status</td>
                                                    <td> Absences </td>
                                                    <td> Tardiness </td>
                                                    <td> Excused </td>
                                                    
                                                    <!-- concat time start and end and days-->
                                                    <!-- <th>Time</th>
                                                        <th>Day/s</th>
                                                        <th>Room</th>
                                                        <th>Time in</th>
                                                        <th>Time out</th>
                                                        <th>Time in Status</th>
                                                        <th>Time out Status</th> -->
                                                    <!-- <th>Action</th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach (getStudentsPerSched($getSched['sched_uid'], $data['date']) as $userData) {
                                                ?>
                                                    <tr>
                                                        <td><?php echo $userData['userIdNo'] ?></td>
                                                        <td><?php echo $userData['firstName'] . ' ' . $userData['lastName'] ?></td>
                                                        <td><?php
                                                            if ($userData['time_in_final'] == $getSched["time_start"] || $userData['time_in_final'] < $getSched["time_start"]) {
                                                                $selected =  "PRESENT";
                                                            } else if (($userData['time_in_final'] > $getSched["time_start"]) && (strtotime($userData['time_in_final']) < strtotime('+' . $getSched["timeAllowance"] . 'minutes', strtotime($getSched["time_start"])))
                                                            ) {
                                                                $selected =  "LATE";
                                                            } else {
                                                                $selected =  "ABSENT";
                                                            }
                                                            echo $selected;
                                                            ?>
                                                        </td>
                                                        <td> <?php echo '1'; ?></td>
                                                        <td> <?php echo '2'; ?></td>
                                                        <td> <?php echo '3'; ?></td>




                                                        <!-- sql query concat lanme and fname -->
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat " style="margin-right: 4em">CLOSE</a>
                                    </div>
                                </div>
                                <!-- modal end -->
    
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>

        </div>

        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, {
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },
                ],
            }, );
        });
    </script>
</body>

</html>