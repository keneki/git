    <!--Import Google Icon Font-->
    <link href="css/fontMaterialize.css" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <!-- additional css -->
    <link type="text/css" rel="stylesheet" href="css/sweetalert2.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/dataTable2.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/materialDesign.css" media="screen,projection" />
    <link href="css/font.css" rel="stylesheet">
    <!-- Custom Css -->
    <link type="text/css" rel="stylesheet" href="css/nav.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" />

    <!-- dataTable -->
    <link rel="stylesheet" href="css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="css/jquery.dataTables.min.css">
    <title>AMS</title>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />