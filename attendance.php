<?php
  require_once 'config.php';
  if(isset($_REQUEST['save'])){
     $timein = $_POST['timein'];
     $timeout = $_POST['timeout'];
     $allowance = $_POST['allowancetime'];
    $status = $_REQUEST['student_stat'];
    $statid = $_REQUEST['id'];
     $insert = "INSERT INTO edit_time (edit_timein, edit_timeout, allowance) VALUES ('$timein','$timeout','$allowance')";
    $attendance_update = "UPDATE table_class_attendance SET student_attendance = '$status' WHERE id = '$statid'";
    if ( mysqli_query($conn, $attendance_update)) {
      echo "New record created successfully";
    }
    else {
      echo "Error: " . $insert . "<br>" . mysqli_error($conn);
    } 
  }
?>  
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Attendance Page</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/attendance-extension.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/jquery.timepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />
	<script type="text/javascript" src="css/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css" />
	<script type="text/javascript" src="js/site.js"></script>
  <link rel="stylesheet" type="text/css" href="css/site.css" />

  <script>
    <!-- Time Picker Scripts-->
    $(function() {
      $('#timeIn').timepicker({
        'timeFormat': 'h:i A',
        'step': 5,
        'forceRoundTime': true
      });
    });
    $(function() {
      $('#timeOut').timepicker({
        'timeFormat': 'h:i A',
        'step': 5,
        'forceRoundTime': true
      });
    });
    <!-- Table Scroll Scripts-->
    jQuery(document).ready(function() {
      jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone')
    });
    <!-- Modal Content-->
    $(document).ready(function(){
      $("#customFile").closest("div").hide();
      $("#remarks").closest("div").hide();
      $("#stat").on("change", function(){
        var v = $(this).val();
        if(v=="Excuse"){
            $("#customFile").closest("div").show();
            $("#remarks").closest("div").show();
        }else{
          $("#customFile").closest("div").hide();
          $("#remarks").closest("div").hide();
        } 
      });
    });

    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
  </script>

  <?php 
    require_once 'config.php';
    $attendance_info = "SELECT * FROM table_student INNER JOIN table_class_attendance
                        ON table_student.id = table_class_attendance.student_id
                        WHERE table_class_attendance.course_id = 2";
       
    $class_info = "SELECT * FROM table_class INNER JOIN table_course
                   ON table_class.id = table_course.class_id
                   WHERE table_course.id = 2";
  ?> 
</head>
  <!-- Content -->
<body class = "bg-info">
  <div class = "container" >
    <div class="row justify-content-center">
      <div class="col-lg-100 bg-light mt-5 p-4 rounded">
        <!-- Time In, Out, and Allowance-->
        <form method="POST" action="attendance.php">
        <div class="form-group row">
          <label for="colFormLabelSm" class="col-xs-3 col-form-label col-form-label-xs font-weight-bold ml-3">Time In:</label>
          <div class="col-sm-2">
            <input id="timeIn" type="text"  style ="width:95px" class="form-control mb-2 mr-1" name="timein"/>
          </div>
          <label for="colFormLabelSm" class="col-xs-2 col-form-label col-form-label-xs font-weight-bold ml-3">Time Out:</label>
          <div class="col-sm-2">  
            <input id="timeOut" type="text" style ="width:95px" class="form-control mb-2 mr-1" name="timeout"/>
          </div>
          <label for="colFormLabelSm" class="col-xs-2 col-form-label col-form-label-xs font-weight-bold ml-3">Allowance:</label>
          <div class="col-sm" style ="width:130px">  
            <select class="form-control" name="allowancetime">
              <option value="5"> 5 MIN. </option>
              <option value="10" > 10 MIN. </option>
              <option value="15"> 15 MIN. </option>
              <option value="20"> 20 MIN. </option>
              <option value="25"> 25 MIN. </option>
              <option value="30"> 30 MIN. </option>
              <option value="0"> NONE </option>
            </select>
          </div>
          <input type="SUBMIT" name="SUBMIT" class="btn button btn-success mb-2 mr-3 ml-3" value="SUBMIT" />
        </div>
        </form> 
        <!-- Tabular Attendance -->
        <?php 
          if($result_info = $conn-> query($class_info)){
            while($row = $result_info-> fetch_assoc()){
              echo"<b> Class course: </b>" . $row["class_code"] . " " . "<b> Schedule: </b>" . $row["course_start"] . " " . $row["course_end"] . " " . $row["day"];
            }
          }
          else{
            echo "0 result";
          }
        ?>
        <div id="table-scroll" class="table-scroll">
          <div class="table-responsive">
            <table class="main-table">
              <thead>
                  <tr>
                      <th class="fixed-side bg-success text-white" >I.D. No.</th>
                      <th class="fixed-side bg-success text-white">Names</th>
                      <th>Date</th>
                      <th class="bg-success text-white">Absences</th>
                      <th class="bg-success text-white">Tardiness</th>
                      <th class="bg-success text-white">Excused</th>
                  </tr>
              </thead>
              <tbody>
                  <?php 
                    if($result_attendance = $conn-> query($attendance_info)){
                      $total_absent = 0;
                      $total_tardy = 0;
                      $total_excuse = 0;
                      while($row = $result_attendance-> fetch_assoc()){
                        echo "<tr>" . '<th class="fixed-side">' . $row["id_number"];
                        echo "</th>" . '<th class="fixed-side">' . $row["student_fname"] . " " . $row["student_minitial"] . $row["student_lname"];  
                        echo "</th><td>";
                        echo '<button type="button" style ="width:80px" class="btn btn-primary"';
                        echo 'data-toggle="modal" data-target="#statusModal">';
                        echo '<a data-toggle="tooltip" data-placement="bottom" title="Time In: <br> Time Out:" data-html="true">';
                        echo $row["student_attendance"] . '</a></button>'; 
                        echo "</td><td>"; 
                            if($row["student_attendance"] == 'Absent'){
                              $total_absent++;
                            }
                            elseif ($row["student_attendance"] == 'Late') {
                              $total_tardy++;
                            }
                            elseif ($row["student_attendance"] == 'Excuse') {
                              $total_excuse++;
                            }
                            else{
                              $total_absent = 0;
                              $total_tardy = 0;
                              $total_excuse = 0;
                            }
                        echo $total_absent; 
                        echo "</td><td>" .  $total_tardy;
                        echo "</td><td>" . $total_excuse;
                        echo "</td></tr>";
                      }
                      echo "</table>";
                    }
                    else{
                      echo "0 result";
                    }
                  ?>
                </tbody>
              </table>

            <div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="statusModalLabel">Edit Student Status:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <select class="form-control" name="student_stat" id="stat">
                      <option value="Present">Present</option>
                      <option value="Absent">Absent</option>
                      <option value="Excuse">Excuse</option>
                    </select>

                    <div class="custom-file mt-lg-3">
                      <input type="file" class="custom-file-input mt-lg-3" id="customFile">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      <embed src="ZeroTwo.png" frameborder="0" width="100%" height="400px">
                    </div>

                    <div class="form-group">
                      <textarea class="form-control" id="remarks" rows="3"  placeholder="Write your remarks here..."></textarea>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
        </div>
      </div>
    </div>
</body>

</html>