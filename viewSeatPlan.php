<?php
include 'model/dbhelper.php';

if (!$_SESSION) {
    header("location: index.php?m='Please login first'");
}

$schedId = $_GET['schedId'];
$getSchedDates = getSchedDates($schedId);
$getSched = getSchedById($schedId);
$getProf = getProfName($schedId);
$getStud = getStudentsPerSched1($schedId);

?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php' ?>

</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <div class="section card-panel" style="margin-bottom: 24px">
                <h4 class="center-align light">Course Details</h4>
                <div class="row">
                    <div class="col s4">
                        <h6>Class Course: <?php echo $getSched["course_code"] ?></h6>
                        <h6>Room Name: <?php echo $getSched["room"] ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Schedule: <?php echo $getSched["time_start"] . '-' . $getSched["time_end"] . ' ' . $getSched['sched']   ?></h6>
                        <h6>Room Type: <?php echo $getSched["room_type"] ?  "Laboratory" : "Classroom"  ?></h6>
                    </div>
                    <div class="col s4">
                        <h6>Group No: <?php echo $getSched["group_no"] ?></h6>
                        <h6>Instructor: <?php echo $getProf["profFirstname"] . ' ' . $getProf["profLastname"] ?></h6>
                    </div>
                </div>
            </div>
            <table width=50% height=50% border=2px>
                <?php
                if (isset($_POST["submit1"])) {
                   
                    $row1 = $_POST["rows"];
                    $column1 = $_POST["columns"];
                    echo $row1;
                    echo $column1;
                    for ($row = 1; $row <= $row1; $row++) {
                        echo ("<tr>");
                        for ($column = 1; $column <= $column1; $column++) {
                            $result = $row + $column;
                            if (($result) % 2 == 0) {
                                echo "<td bgcolor=#FFFFFF> </td>";
                            } else {
                                echo "<td bgcolor=#000000> </td>";
                            }
                        }
                        echo ("</tr>");
                    }
                }

                ?>
            </table>
        </div>

        <!-- end -->
    </main>
    <!-- modal -->
    <!-- Modal Structure -->

    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.datepicker');
            var instances = M.Datepicker.init(elems, {
                defaultDate: new Date(),
                setDefaultDate: true
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable({
                // items per page
                "pageLength": 10,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5]
                    //     }
                    // },
                ],
            }, );
        });
    </script>
</body>

</html>