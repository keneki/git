-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2020 at 10:50 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cams`
--

-- --------------------------------------------------------

--
-- Table structure for table `edit_time`
--

CREATE TABLE `edit_time` (
  `id` int(11) NOT NULL,
  `edit_timein` time NOT NULL,
  `edit_timeout` time NOT NULL,
  `allowance` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edit_time`
--

INSERT INTO `edit_time` (`id`, `edit_timein`, `edit_timeout`, `allowance`) VALUES
(4, '12:00:00', '12:05:00', 10),
(5, '12:00:00', '12:05:00', 10),
(6, '12:00:00', '12:05:00', 5),
(7, '12:10:00', '12:20:00', 5),
(8, '00:00:00', '00:00:00', 5),
(9, '00:00:00', '00:00:00', 5),
(10, '00:00:00', '00:00:00', 5),
(11, '00:00:00', '00:00:00', 5),
(12, '00:00:00', '00:00:00', 5),
(13, '00:00:00', '00:00:00', 5),
(14, '00:00:00', '00:00:00', 5),
(15, '12:20:00', '01:00:00', 30);

-- --------------------------------------------------------

--
-- Table structure for table `table_class`
--

CREATE TABLE `table_class` (
  `id` int(11) NOT NULL,
  `class_desc` varchar(100) DEFAULT NULL,
  `class_code` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_class`
--

INSERT INTO `table_class` (`id`, `class_desc`, `class_code`) VALUES
(1, 'Web Development', 'CpE 76N'),
(2, 'Project Management', 'CpE 74N');

-- --------------------------------------------------------

--
-- Table structure for table `table_class_attendance`
--

CREATE TABLE `table_class_attendance` (
  `id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `student_attendance` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_class_attendance`
--

INSERT INTO `table_class_attendance` (`id`, `course_id`, `student_id`, `teacher_id`, `student_attendance`) VALUES
(1, 2, 1, 1, 'Excuse'),
(2, 2, 2, 1, 'Late'),
(3, 1, 1, 2, 'Excuse'),
(4, 2, 2, 2, 'Absent');

-- --------------------------------------------------------

--
-- Table structure for table `table_course`
--

CREATE TABLE `table_course` (
  `id` int(11) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `course_start` time DEFAULT NULL,
  `course_end` time DEFAULT NULL,
  `day` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_course`
--

INSERT INTO `table_course` (`id`, `class_id`, `course_start`, `course_end`, `day`) VALUES
(1, 1, '07:30:00', '10:30:00', 'TTH'),
(2, 2, '01:30:00', '04:30:00', 'F');

-- --------------------------------------------------------

--
-- Table structure for table `table_faculty`
--

CREATE TABLE `table_faculty` (
  `id` int(11) NOT NULL,
  `id_number` int(11) NOT NULL,
  `teacher_fname` varchar(50) NOT NULL,
  `teacher_minitial` varchar(50) NOT NULL,
  `teacher_lname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_faculty`
--

INSERT INTO `table_faculty` (`id`, `id_number`, `teacher_fname`, `teacher_minitial`, `teacher_lname`) VALUES
(1, 15000000, 'Alvin Joseph', 'S.', 'Macapagal'),
(2, 15101010, 'Jan Dave', 'A. ', 'Campanera');

-- --------------------------------------------------------

--
-- Table structure for table `table_student`
--

CREATE TABLE `table_student` (
  `id` int(11) NOT NULL,
  `student_fname` varchar(50) NOT NULL,
  `student_minitial` varchar(50) NOT NULL,
  `student_lname` varchar(50) NOT NULL,
  `id_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_student`
--

INSERT INTO `table_student` (`id`, `student_fname`, `student_minitial`, `student_lname`, `id_number`) VALUES
(1, 'Julia Fe', 'I.', 'Tabaco', 15103342),
(2, 'Kenneth Kent ', 'S.', 'Flores', 15109999);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `edit_time`
--
ALTER TABLE `edit_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_class`
--
ALTER TABLE `table_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_class_attendance`
--
ALTER TABLE `table_class_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `table_course`
--
ALTER TABLE `table_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class_id` (`class_id`);

--
-- Indexes for table `table_faculty`
--
ALTER TABLE `table_faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_student`
--
ALTER TABLE `table_student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `edit_time`
--
ALTER TABLE `edit_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `table_class`
--
ALTER TABLE `table_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_class_attendance`
--
ALTER TABLE `table_class_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table_course`
--
ALTER TABLE `table_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_faculty`
--
ALTER TABLE `table_faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `table_student`
--
ALTER TABLE `table_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `table_class_attendance`
--
ALTER TABLE `table_class_attendance`
  ADD CONSTRAINT `table_class_attendance_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `table_course` (`id`),
  ADD CONSTRAINT `table_class_attendance_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `table_student` (`id`),
  ADD CONSTRAINT `table_class_attendance_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `table_faculty` (`id`);

--
-- Constraints for table `table_course`
--
ALTER TABLE `table_course`
  ADD CONSTRAINT `table_course_ibfk_1` FOREIGN KEY (`class_id`) REFERENCES `table_class` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
