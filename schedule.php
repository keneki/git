<?php
 include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
$profList = getAllProf();
$courseList = getAllCourse();
$schedList=getAllSched();
?>
<!DOCTYPE html>
<html>

<head>

    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->
            <form class="form-horizontal" action="" method="post" name="uploadCSV"
            enctype="multipart/form-data">
    <div class="input-row">
        <a class="col-md-4 control-label">Choose CSV File</a> <input
            type="file" name="file" id="file" accept=".csv">
        <button type="submit" class="btn btn-primary" id="submit" name="import"
            class="btn-submit">Import</button>
        <br />

    </div>
    <div id="labelError"></div>
</form>
        <div class="section" style="margin: 2rem;">
            <a class="waves-effect waves-light btn modal-trigger" href="#modal1"><i
                    class="material-icons right">add</i>Schedule</a>
        </div>

        <div class="section" style="margin: 2rem;">
            <?php
                if($_GET['status']=="success_delete"){echo "<div class='card-panel teal lighten-5 teal-text'>Deleted sucessfully</div>";}
                if($_GET['status']=="success_adding"){echo "<div class='card-panel green lighten-5 green-text'>Added sucessfully</div>";}
                if($_GET['status']=="failed_adding"){echo "<div class='card-panel red lighten-5 red-text'>Failed adding</div>";}
                if($_GET['status']=="success_updating"){echo "<div class='card-panel green lighten-5 green-text'>Update sucessfully</div>";}
                if($_GET['status']=="failed_updating"){echo "<div class='card-panel red lighten-5 red-text'>Failed updating</div>";}
        ?>
<?php
$conn = mysqli_connect("localhost", "root", "", "ams_db");

if (isset($_POST["import"])) {
    
    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
            $sqlInsert = "INSERT into tbl_schedule (sched_uid,course_uid,prof_uid,time_start,time_end,sched,room)
                   values ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "','" . $column[3] . "','" . $column[4] . "','" . $column[5] . "','" . $column[6] . "')";
            $result = mysqli_query($conn, $sqlInsert);
            
            if (! empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
        }
    }
}
?>

<?php

$sqlSelect = "SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_prof prf WHERE crs.course_uid = sch.course_uid AND prf.prof_uid = sch.prof_uid";
$result = mysqli_query($conn, $sqlSelect);
            
if (mysqli_num_rows($result) > 0) {
?>

<table id="example" class="display" style="width:100%">

    <thead>
        <tr>
            <!-- concat intructor name -->
            <th>Subject Code</th>
            <th>Descriptive Title</th>
            <th>Instructor</th>
            <th>Section</th>
            <th>Units</th>
            <th>Time</th>
            <th>Day/s</th>
            <th>Room</th>
            <!-- concat time start and end -->
            <th>Action</th>

        </tr>
        
    </thead>
    <?php

	while ($schedList = mysqli_fetch_array($result)) {
    ?>

    <tbody>
        <tr>
        <td><?php echo $schedList['subject_code'];?></td>
        <td><?php echo $schedList['desc_title'];?></td>
        <td><?php echo $schedList['profLastname'].", ".$schedList['profFirstname'];?></td>
        <td><?php echo $schedList['section'];?></td>
        <td><?php echo $schedList['units'];?></td>
        <td><?php echo date('h:iA',strtotime($schedList['time_start']))."-".date('h:iA', strtotime($schedList['time_end']));?>
        <td><?php $scheduleList= $schedList['sched'];
        print_r($scheduleList);
                        // foreach($scheduleList as $key){
                        //     echo $key;
                        // }
?>
    </td>
    <td><?php echo $schedList['room'];?></td>
    <td>
        <a class="waves-effect waves-light btn"
             href="editSched.php?id=<?php echo $schedList['sched_uid']?>"><i
            class="material-icons">create</i></a>
            <button onclick="test(<?php echo $schedList['sched_uid']?>)"
            class="waves-effect waves-light red btn"><i class="material-icons">delete</i></button>
            
    </td>
        </tr>
     <?php
     }
    
     ?>
    </tbody>
    
</table>
<?php } ?>

        </div>
        <!-- end -->
    </main>
    <!-- modal -->


    <!-- Modal Structure -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h4>Schedule</h4>
            <div class="row">
                <form action="controller/schedCtrl.php" method="POST">
                    <p>Basic Info</p>
                    <div class="input-field col s12">
                        <select name="course_uid">
                            <option value="" disabled selected>Choose course</option>
                            <?php
                                foreach($courseList as $courseData){
                            ?>
                            <option value="<?php echo $courseData['course_uid']?>?">
                                <?php echo $courseData['subject_code']." - ".$courseData['desc_title']." - ".$courseData['section']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Course</label>
                    </div>
                    <div class="input-field col s12">
                        <select name="prof_uid">
                            <option value="" disabled selected>Choose instructors</option>
                            <?php
                                foreach($profList as $profData){
                            ?>
                            <option value="<?php echo $profData['prof_uid']?>?">
                                <?php echo $profData['profLastname'].", ".$profData['profFirstname']?> </option>
                            <?php
                                }
                            ?>
                        </select>
                        <label>Instructors</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_start" class="timepicker" name="time_start" type="text" class="validate">
                        <label for="time_start">Time Start</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="time_end" class="timepicker" name="time_end" type="text" class="validate">
                        <label for="time_end">Time End</label>
                    </div>
                    <div class="col s12 center">
                    <label for="">Day/s</label>
                        <p>
                            <label style="margin-right :1em;">
                                <input type="checkbox" name="sched_list[]" value="Monday">
                                <span>Monday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Tuesday">
                                <span>Tuesday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Wednesday">
                                <span>Wednesday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Thursday">
                                <span>Thursday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Friday">
                                <span>Friday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Saturday">
                                <span>Saturday</span>
                            </label>
                            <label >
                                <input type="checkbox" name="sched_list[]" value="Sunday">
                                <span>Sunday</span>
                            </label>
                        </p>
                    </div>
                    <div class="input-field col s12">
                        <input id="room" name="room" type="text" pattern="[a-zA-Z0-9\s]+" class="validate">
                        <label for="room">Room</label>
                    </div>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-red  red white-text btn-flat">Cancel</a>
            <button id="btnAddBldg" class="btn waves-effect waves-light" type="submit" name="btnAddSched">Add
                Schedule
                <i class="material-icons right">add</i>
            </button>
        </div>
        </form>

    </div>
    <!-- modal end -->

    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->
    <script>
        $(document).ready(function () {
            $('.modal').modal();
        });
    </script>
    <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                // items per page
                "pageLength": 20,
                dom: 'Bfrtip',
                buttons: [
                    // {
                    //     extend: 'copy',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5, 6]
                    //     }
                    // },

                    // {
                    //     extend: 'csv',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5, 6]
                    //     }
                    // },

                    // {
                    //     extend: 'excel',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4, 5, 6]
                    //     }
                    // },

                    {
                        text: 'Export to PDF',
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
        });
    </script> -->
    <script type="text/javascript">
	$(document).ready(
	function() {
		$("#frmCSVImport").on(
		"submit",
		function() {

			$("#response").attr("class", "");
			$("#response").html("");
			var fileType = ".csv";
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+("
					+ fileType + ")$");
			if (!regex.test($("#file").val().toLowerCase())) {
				$("#response").addClass("error");
				$("#response").addClass("display-block");
				$("#response").html(
						"Invalid File. Upload : <b>" + fileType
								+ "</b> Files.");
				return false;
			}
			return true;
		});
	});
</script>
    <script>
        function test(id) {
            Swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    window.location.href = `controller/deleteSchedule.php?id=${id}`;
                }
            })

        }
    </script>
</body>

</html>