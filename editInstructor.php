<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_USER_NOTICE);
include 'model/dbhelper.php';
 if(!$_SESSION){
    header("location: index.php?m='Please login first'");
}
//$course = getSched($_GET['id']);
// print_r($course);
$prof = getProf($_GET['$id']);
$profList = getAllProf();
//$courseList = getAllCourse();
$deptList = getAllDept();

?>
<!DOCTYPE html>
<html>

<head>
    <?php include 'head.php'?>
</head>

<body>
    <!--  -->
    <!-- NAVBAR -->
    <?php include 'header.php'; ?>

    <main>
        <!-- start -->
        <div class="section" style="margin: 2rem;">
            <!-- Modal Trigger -->

        </div>
        <div class="section" style="margin: 2rem;">
            <form action="controller/editInstructor.php" method="post">
            <div class="row">
                    <div class="input-field col s12">
                        <input id="profCardId" name="profCardId" type="text" class="validate"
                        value="<?php echo $prof['profCardId']?>">
                        <label for="profCaardId">Card ID</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profFirstname" name="profFirstname" type="text" class="validate"
                        value="<?php echo $prof['profFirstname']?>">
                        <label for="profFirstname">First Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profLastname" name="profLastname" type="text" class="validate"
                        value="<?php echo $prof['profLastname']?>">
                        <label for="profLastname">Last Name</label>
                    </div>
                    <div class="input-field col s12">
                        <input id="profIdNum" name="profIdNum" type="text" class="validate"
                        value="<?php echo $prof['profIdNum']?>">
                        <label for="profIdNum">ID Number</label>
                    </div>
                    <div class="input-field col s12">
                        <select name="dept_name">
                            <option value="" disabled selected >Department</option>
                            <?php
                                foreach($deptList as $deptData){
                            ?>
                            <option value="<?php echo $deptData['dept_name']?>">
                                <?php echo $deptData['dept_name']?> </option>
                            <?php
                                }
                            ?>
                            
                        </select>
                    </div>
                    <div class="input-field col s12">
                        <input class="btn" type="submit" value="Update" name="update" id="update">
                    </div>
            </div> 
            </form>
        </div>
        <!-- end -->
    </main>
    <!-- modal -->




    <!--  -->
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!-- additional js -->
    <script type="text/javascript" src="js/sweetalert2.min.js"></script>
    <script type="text/javascript" src="js/util.js"></script>
    <!-- dataTable js -->
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.buttons.min.js"></script>
    <script src="js/buttons.print.min.js"></script>
    <script src="js/buttons.flash.min.js"></script>
    <script src="js/buttons.html5.min.js"></script>
    <script src="js/jszip.min.js"></script>
    <script src="js/pdfmake.min.js"></script>
    <script src="js/vfs_fonts.js"></script>
    <!-- custom js -->

</body>
</html>