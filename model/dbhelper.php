<?php
session_start();

function dbconn()
{
    try {
        return new PDO("mysql:hostname=localhost;dbname=ams_db", "root", "");
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

function destroy()
{
    return null;
}

function login($username, $password)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users INNER JOIN tbl_dept ON tbl_users.deptId = tbl_dept.deptId where userUsername = ? AND userPassword = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($username, $password));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}



function addCourse($data)
{
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_course(subject_code,desc_title,section,units) VALUES(?,?,?,?)";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}

function addAccount($data)
{
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_users(userName,userUsername,userPassword,userType) VALUES(?,?,?,?)";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}

function addProf($data)
{
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_prof(profFirstname,profLastname,profCardId,profIdNum,dept_name) VALUES(?,?,?,?,?)";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}
function addSched($data)
{
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_schedule(course_uid,prof_uid,time_start,time_end,sched,room) VALUES(?,?,?,?,?,?)";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}
function addLogs($data)
{
    $dbconn = dbconn();
    $sql = "INSERT INTO tbl_logs(sched_uid, date, logs_status) VALUES (?,CURRENT_DATE,?)";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = null;
}

// function addLogss($data){
//     $dbconn = dbconn();
//     $sql = "INSERT INTO tbl_logs(sched_uid, date, logs_statuss) VALUES (?,CURRENT_DATE,?)";
//     $stmt=$dbconn->prepare($sql);
//     $stmt->execute($data);
//     $dbconn=null;

// }
function updateCurrentLog($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_logs SET time_out = CURRENT_TIMESTAMP, logs_statuss = ? WHERE logs_id = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}

// -retrieve

function selectIfInstructorHasLog($schedUid)
{
    $dbconn = dbconn();
    $sql = "SELECT * from tbl_logs l, tbl_schedule s, tbl_prof p where p.prof_uid = s.prof_uid AND s.sched_uid = l.sched_uid AND s.sched_uid = ? AND l.date = CURRENT_DATE AND s.time_start <= CURRENT_TIME AND s.time_end + INTERVAL 10 MINUTE > CURRENT_TIME";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedUid));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = null;
    return $row;
}
function updateSched($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_schedule SET course_uid=? , prof_uid=?,time_start=?,time_end=?,sched=? ,room=? WHERE sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}

function updateInstructor($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_prof SET prof_uid=? , profFirstname=?,profLastname=?,profCardId=?,profIdNum=? ,dept_name=? WHERE prof_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}




function selectClass($cardId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule s,tbl_prof p , tbl_course c WHERE s.prof_uid = p.prof_uid AND p.profCardId = ? AND s.course_uid = c.course_uid  AND s.time_start - INTERVAL 15 MINUTE <= CURRENT_TIME AND s.time_end + INTERVAL 10 MINUTE >= CURRENT_TIME
    ";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($cardId));
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = null;
    return $row;
}

function checkOldPassword($data)
{
    $dbconn = dbconn();
    $sql = "SELECT COUNT(*) FROM tbl_users where userId = ? AND userPassword = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $row = $stmt->fetchColumn();
    $dbconn = destroy();
    return $row;
}
function getAllCourse()
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_course";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getAllCourse1()
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tblcourse crs, tbl_schedule sch WHERE crs.course_uid = sch.course_uid";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getSchedProf($prof_uid)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule INNER JOIN tbl_course ON tbl_schedule.course_uid = tbl_course.course_uid WHERE prof_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($prof_uid));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}


function getSchedStudents($userId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_attendance att WHERE crs.course_uid = sch.course_uid AND att.sched_uid = sch.sched_uid AND userId = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($userId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
// function getSchedStudents($userId)
// {
//     $dbconn = dbconn();
//     $sql = "SELECT * FROM tbl_schedule INNER JOIN tbl_attendance ON tbl_schedule.sched_uid = tbl_attendance.sched_uid WHERE userId = ?";
//     $stmt = $dbconn->prepare($sql);
//     $stmt->execute(array($userId));
//     $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
//     $dbconn = destroy();
//     return $rows;
// }

function getStudentsPerSched1($schedId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_enrolled INNER JOIN tbl_users ON tbl_enrolled.userId = tbl_users.userId WHERE sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getStudent($userId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users INNER JOIN tbl_dept ON tbl_users.deptId = tbl_dept.deptId where userId = ? ";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($userId));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getStudentsPerSched($attDayId)
{
    $dbconn = dbconn();
    $sql = "SELECT ta1.* FROM tbl_attendance ta1 LEFT JOIN  tbl_attendance ta2  ON (ta1.userId = ta2.userId AND ta1.time_in > ta2.time_in) WHERE ta1.att_day_id = ? AND ta2.attendId IS NULL";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($attDayId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getStudentPerDate($schedId, $date)
{
    $dbconn = dbconn();
    $sql = "SELECT *,min(tbl_attendance.time_in) as time_in_final, max(tbl_attendance.time_out) as time_out_final FROM tbl_users INNER JOIN tbl_attendance ON tbl_users.userId = tbl_attendance.userId WHERE sched_uid = ? AND  date = ? GROUP BY tbl_users.userId";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId, $date));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getStudentsPerClass($schedId)
{
    $dbconn = dbconn();
    $sql = "SELECT tbl_users.* FROM tbl_enrolled INNER JOIN tbl_schedule ON tbl_enrolled.sched_uid = tbl_schedule.sched_uid INNER JOIN tbl_users ON tbl_enrolled.userId = tbl_users.userId WHERE tbl_schedule.sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getSchedById($schedId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule INNER JOIN  tbl_course ON tbl_schedule.course_uid = tbl_course.course_uid WHERE sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    //echo $rows["course_uid"];
    return $rows;
}
function getProfName($schedId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule INNER JOIN  tbl_prof ON tbl_schedule.prof_uid = tbl_prof.prof_uid WHERE sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    //echo $rows["course_uid"];
    return $rows;
}

function getSchedDates($schedId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM `tbl_attendance_day` INNER JOIN tbl_schedule ON tbl_attendance_day.sched_uid = tbl_schedule.sched_uid WHERE tbl_attendance_day.sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getSchedStatus($schedId, $userId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_status INNER JOIN tbl_attendance ON tbl_attendance.status = tbl_status.statusId WHERE tbl_attendance.sched_uid = ? AND tbl_attendance.userId = ?  GROUP BY date";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($schedId, $userId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}


function getAttendeesCount($date, $sched_uid)
{
    $dbconn = dbconn();
    $sql = "SELECT  *, COUNT(DISTINCT ta.userId ) as count FROM tbl_attendance_day td RIGHT JOIN tbl_attendance ta ON  td.att_day_id = ta.att_day_id WHERE date = ? AND td.sched_uid = ? AND ta.status != 3 ";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($date, $sched_uid));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getStudentStatus($userId)
{
    $dbconn = dbconn();
    $sql = "SELECT ta1.* FROM tbl_attendance ta1 LEFT JOIN tbl_attendance ta2 ON (ta1.att_day_id = ta2.att_day_id AND ta1.time_in > ta2.time_in) INNER JOIN tbl_attendance_day ON  WHERE ta1.userId = ? AND  ta2.attendId IS NULL";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($userId));
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}



function getStatus($statusId)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_status  WHERE statusId = ? ";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($statusId));
    $rows = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}


function getAllUsers()
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getAllSched()
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_prof prf WHERE crs.course_uid = sch.course_uid AND prf.prof_uid = sch.prof_uid";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
function getSched($id)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_schedule sch,tbl_course crs,tbl_prof prf WHERE crs.course_uid = sch.course_uid AND prf.prof_uid = sch.prof_uid AND sch.sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($id));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}


function getAllProf()
{
    $dbconn = dbconn();
    $sql = "SELECT *,tbl_prof.dateCreated AS prof_dateCreated FROM tbl_prof";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // echo "{$row['profFirstname']}";
    // echo " {$row['profLastname']}";
    $dbconn = destroy();
    return $rows;
}

function getProf($id)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_prof";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($id));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}

function getAllDept()
{
    $dbconn = dbconn();
    $sql = "SELECT *,tbl_dept.dateCreated AS dept_dateCreated FROM tbl_dept";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // echo "{$row['profFirstname']}";
    // echo " {$row['profLastname']}";
    $dbconn = destroy();
    return $rows;
}

function getEmail($data)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_users where userUsername =?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute(array($data));
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $row;
}
function getAllLogs()
{
    $dbconn = dbconn();
    $sql = "SELECT *,CONCAT(profLastname,', ',profFirstName)AS prof_name FROM tbl_logs l, tbl_course c, tbl_prof p, tbl_schedule s WHERE l.sched_uid = s.sched_uid AND p.prof_uid = s.prof_uid AND c.course_uid = s.course_uid";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}
// ---------- Old ref
function getAllPerson2()
{
    $dbconn = dbconn();
    $sql = "SELECT idno,fname,lname,email,status FROM tbl_person";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $dbconn = destroy();
    return $rows;
}

function getPerson($data)
{
    $dbconn = dbconn();
    $sql = "SELECT * FROM tbl_person WHERE idno = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $row = $stmt->fetch();
    $dbconn = destroy();
    return $row;
}

function updatePerson($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_person SET fname=? , lname=?,email=?,password=?,status=? WHERE idno = ?";
    $stmt = $dbconn->prepare($sql);
    $stmt->execute($data);
    $dbconn = destroy();
}
function updatePassword($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_users SET userPassword= ? WHERE userId = ?";
    $stmt = $dbconn->prepare($sql);
    $ok = $stmt->execute($data);
    $dbconn = destroy();
    return $ok;
}

function updateStatus($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_attendance SET status = ? WHERE attendId = ?";
    $stmt = $dbconn->prepare($sql);
    $ok = $stmt->execute($data);
    $dbconn = destroy();
    return $ok;
}

function updateSchedTime($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_attendance_day SET time_start_day = ?, time_end_day = ?, time_allowance = ?  WHERE att_day_id = ?";
    $stmt = $dbconn->prepare($sql);
    $ok = $stmt->execute($data);
    $dbconn = destroy();
    return $ok;
}
// function updateStatus($data)
// {
//     $dbconn = dbconn();
//     $sql = "UPDATE tbl_attendance SET status=? WHERE tbl_attendance.userId = ?";
//     $stmt = $dbconn->prepare($sql);
//     $stmt->execute($data);
//     $dbconn = destroy();
// }
function updateAllowance($data)
{
    $dbconn = dbconn();
    $sql = "UPDATE tbl_schedule SET timeAllowance=? WHERE `tbl_schedule`.`sched_uid` = ?";
    $stmt = $dbconn->prepare($sql);
    $ok = $stmt->execute($data);
    $dbconn = destroy();
    return $ok;
}


function deleteInstructor($id)
{
    $isDeleted = false;

    $dbconn = dbconn();
    $sql = "DELETE FROM tbl_prof WHERE prof_uid = ?";
    $stmt = $dbconn->prepare($sql);
    if ($stmt->execute(array($id))) {
        $isDeleted = true;
    }
    $dbconn = destroy();
    return $isDeleted;
}

function deleteCourse($id)
{
    $isDeleted = false;

    $dbconn = dbconn();
    $sql = "DELETE FROM tbl_course WHERE course_uid = ?";
    $stmt = $dbconn->prepare($sql);
    if ($stmt->execute(array($id))) {
        $isDeleted = true;
    }
    $dbconn = destroy();
    return $isDeleted;
}
function deleteSchedule($id)
{
    $isDeleted = false;

    $dbconn = dbconn();
    $sql = "DELETE FROM tbl_schedule WHERE sched_uid = ?";
    $stmt = $dbconn->prepare($sql);
    if ($stmt->execute(array($id))) {
        $isDeleted = true;
    }
    $dbconn = destroy();
    return $isDeleted;
}
