-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 31, 2019 at 11:02 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ams_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bldg`
--

CREATE TABLE `tbl_bldg` (
  `bldg_uid` int(11) NOT NULL,
  `bldg_code` varchar(255) NOT NULL,
  `bldg_name` varchar(255) NOT NULL,
  `bldg_location` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_bldg`
--

INSERT INTO `tbl_bldg` (`bldg_uid`, `bldg_code`, `bldg_name`, `bldg_location`, `dateCreated`) VALUES
(1, 'Main', 'Main Bldg', 'Saciangko', '2019-07-31 12:41:26'),
(2, 'test', 'test', 'test', '2019-07-31 17:30:06'),
(3, 'asd', 'asd', 'asd', '2019-07-31 17:32:44'),
(4, 'asdasd', 'asdasd', 'asdas', '2019-07-31 17:34:18'),
(5, 'asdasqweqwe', 'dasdasdqweqw', 'qweqwe', '2019-07-31 17:34:31'),
(6, 'Gbuilding', 'Gotianuy Bldg', 'Kalubihan', '2019-07-31 18:05:05'),
(7, 'E', 'Emall', 'Emall', '2019-07-31 19:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_class`
--

CREATE TABLE `tbl_class` (
  `class_uid` int(11) NOT NULL,
  `class_code` varchar(255) NOT NULL,
  `class_section` varchar(255) NOT NULL,
  `class_start` time NOT NULL,
  `class_end` time NOT NULL,
  `class_sched` varchar(255) NOT NULL,
  `crs_uid` int(11) NOT NULL,
  `prof_uid` int(11) NOT NULL,
  `room_uid` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_class`
--

INSERT INTO `tbl_class` (`class_uid`, `class_code`, `class_section`, `class_start`, `class_end`, `class_sched`, `crs_uid`, `prof_uid`, `room_uid`, `dateCreated`) VALUES
(1, 'class code1', '1', '08:00:00', '09:00:00', 'TTH', 1, 1, 1, '2019-07-31 12:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `crs_uid` int(11) NOT NULL,
  `dept_uid` int(11) NOT NULL,
  `crs_code` varchar(255) NOT NULL,
  `crs_title` varchar(255) NOT NULL,
  `crs_desc` varchar(255) NOT NULL,
  `crs_credit` int(11) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`crs_uid`, `dept_uid`, `crs_code`, `crs_title`, `crs_desc`, `crs_credit`, `dateCreated`) VALUES
(1, 1, 'ITELECPHP2', 'PHP2', 'php ', 6, '2019-07-31 20:49:53'),
(2, 1, 'FREEELECNIH1', 'Nihonggo1', 'japanses classes', 3, '2019-07-31 20:49:56'),
(3, 2, 'test code', 'trest title', 'test ', 3, '2019-07-31 20:51:54'),
(4, 1, 'FREEELECPM', 'PM', 'Project management', 3, '2019-07-31 20:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dept`
--

CREATE TABLE `tbl_dept` (
  `dept_uid` int(11) NOT NULL,
  `dept_code` varchar(255) NOT NULL,
  `dept_name` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_dept`
--

INSERT INTO `tbl_dept` (`dept_uid`, `dept_code`, `dept_name`, `dateCreated`) VALUES
(1, 'BSIT', 'Info Tech', '2019-07-31 12:44:03'),
(2, 'BSCRIM', 'Criminology', '2019-07-31 19:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prof`
--

CREATE TABLE `tbl_prof` (
  `prof_uid` int(11) NOT NULL,
  `prof_id` int(11) NOT NULL,
  `dept_uid` int(11) NOT NULL,
  `prof_lname` varchar(255) NOT NULL,
  `prof_fname` varchar(255) NOT NULL,
  `prof_mi` varchar(1) NOT NULL,
  `prof_bday` date NOT NULL,
  `prof_contact` varchar(255) NOT NULL,
  `prof_address` varchar(255) NOT NULL,
  `prof_email` varchar(255) NOT NULL,
  `prof_password` varchar(255) NOT NULL,
  `prof_status` varchar(255) NOT NULL DEFAULT 'active',
  `dateStarted` date NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_prof`
--

INSERT INTO `tbl_prof` (`prof_uid`, `prof_id`, `dept_uid`, `prof_lname`, `prof_fname`, `prof_mi`, `prof_bday`, `prof_contact`, `prof_address`, `prof_email`, `prof_password`, `prof_status`, `dateStarted`, `dateCreated`) VALUES
(1, 15387467, 1, 'DENNIS', 'DURANO', 'D', '1975-02-05', '09991083678', 'test address', 'test@test', 'test', 'active', '2019-07-31', '2019-07-31 12:48:43'),
(2, 123123123, 1, 'asdasd', 'asdasd', 'S', '2019-08-01', 'asdasdas', 'asdasdas', 'asdasdsad@asdasdsa', 'asdasdasdasd', 'active', '2019-08-01', '2019-07-31 19:59:58'),
(3, 2147483647, 2, 'test', 'test', 'T', '2019-08-01', '123123123', 'qrwdasdsa', 'email@email', 'test', 'active', '2019-08-01', '2019-07-31 20:07:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_room`
--

CREATE TABLE `tbl_room` (
  `room_uid` int(11) NOT NULL,
  `bldg_uid` int(11) NOT NULL,
  `room_code` varchar(255) NOT NULL,
  `room_type` varchar(255) NOT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_room`
--

INSERT INTO `tbl_room` (`room_uid`, `bldg_uid`, `room_code`, `room_type`, `dateCreated`) VALUES
(1, 1, '537', 'MMR', '2019-07-31 12:41:46'),
(2, 1, 'rm537', 'Lab', '2019-07-31 18:02:19'),
(3, 6, 'rm244', 'AVR', '2019-07-31 18:05:24'),
(4, 2, 'test room code', 'test room type', '2019-07-31 18:47:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_bldg`
--
ALTER TABLE `tbl_bldg`
  ADD PRIMARY KEY (`bldg_uid`);

--
-- Indexes for table `tbl_class`
--
ALTER TABLE `tbl_class`
  ADD PRIMARY KEY (`class_uid`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`crs_uid`);

--
-- Indexes for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  ADD PRIMARY KEY (`dept_uid`);

--
-- Indexes for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  ADD PRIMARY KEY (`prof_uid`),
  ADD UNIQUE KEY `prof_id` (`prof_id`);

--
-- Indexes for table `tbl_room`
--
ALTER TABLE `tbl_room`
  ADD PRIMARY KEY (`room_uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_bldg`
--
ALTER TABLE `tbl_bldg`
  MODIFY `bldg_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_class`
--
ALTER TABLE `tbl_class`
  MODIFY `class_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `crs_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_dept`
--
ALTER TABLE `tbl_dept`
  MODIFY `dept_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_prof`
--
ALTER TABLE `tbl_prof`
  MODIFY `prof_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_room`
--
ALTER TABLE `tbl_room`
  MODIFY `room_uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
