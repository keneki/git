<?php
if(!empty($_FILES['csv_file']['name'])){
    $file_data = fopen($FILES['csv_file']['name'],'r');
    fgetcsv($file_data);
    while($row = fgetcsv($file_data)){
        $data[]=array(
            'prof_uid'          => $row[0],
            'profFirstname'     => $row[1],
            'profLastname'      => $row[2],
            'profCardId'        => $row[3],
            'profIdNum'         => $row[4],
            'dept_name'         => $row[5],
        );
    }
    echo json_encode($data);
}
?>