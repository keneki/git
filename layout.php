<style type='text/css'>
    .foo {
        float: left;
        width: 75px;
        height: 75px;
        margin: 5px;
        border: 3px solid rgba(0, 0, 0, .2);
        text-align: center;
        margin-left: 250px;
    }
    .box {
        float: left;
        width: 75px;
        height: 75px;
        margin: 5px;
        border: 3px solid rgba(0, 0, 0, .2);
        text-align: center;
    }

    .whiteboard {
        float: left;
        width: 75%;
        height: 25px;
        margin: 5px;
        border: 3px solid rgba(0, 0, 0, .2);
        text-align: center;

    }

    .teacherTable {
        float: left;
        width: 150px;
        height: 75px;
        margin: 5px;
        border: 3px solid rgba(0, 0, 0, .2);
        text-align: center;

    }

    .blue {
        background: #13b4ff;
    }

    .purple {
        background: #ab3fdd;
    }

    .wine {
        background: #ae163e;
    }
</style>

<div>
    <div class="whiteboard " align-center> WHITE BOARD</div>
</div> <br><br> <br><br>
<div>

    <div class="teacherTable " align-center> Teacher's Table </div>
</div> <br><br><br><br><br>
<div>

    <div>
        <div class="foo " align-center> </div>
        <div class="box " align-center> </div>
        <div class="box " align-center> </div>
        <div class="box " align-center> </div>
        <div class="foo " align-center> </div>
        <div class="box " align-center> </div>
        <div class="box " align-center> </div>
        <div class="box " align-center> </div>
    </div>
</div>